import Poly
import numpy as np

'''
legengre(n) : return nth legendre polynomial
'''
def legendre(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)


    l = Poly.Polynomial([-1, 0, 1])                         # x^2 - 1

    l_ = Poly.Polynomial([1])
    for i in range(n):                                      # (x^2 - 1)^ n
        l_ = l_ * l

    for i in range(n):                                      # dn/dnx ((x^2 - 1)^n) ie. nth derivative of ((x^2 - 1)^n)
        l_ = l_.derivative()

    l_ = l_* float(1/(np.power(2, n) * np.math.factorial(n)))       # 1/(2^n*n!) * nth derivative of (x^2 - 1)^n ie. nth legendre polynomial
    
    return l_

#Test cases
# print(legendre(0))
# print(legendre(1))
# print(legendre(2))
# print(legendre(3))
# print(legendre(4))