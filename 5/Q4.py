import Poly
import numpy as np
import scipy.integrate as inte
import matplotlib.pyplot as plt

'''
legengre(n) : return nth legendre ploynomial
'''
def legendre(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    l = Poly.Polynomial([-1, 0, 1])                         # x^2 - 1

    l_ = Poly.Polynomial([1])
    for i in range(n):                                      # (x^2 - 1)^ n
        l_ = l_ * l

    for i in range(n):                                      # dn/dnx ((x^2 - 1)^n) ie. nth derivative of ((x^2 - 1)^n)
        l_ = l_.derivative()

    l_ = l_* float(1/(np.power(2, n) * np.math.factorial(n)))       # 1/(2^n*n!) * nth derivative of (x^2 - 1)^n ie. nth legendre polynomial
    
    return l_

'''
f(x) : return e^x
'''
def f(x):
    return np.power(np.e, x)

'''
phiF(l) : retuns a function g(x) = Ln(x)*e^x where Ln is the nth legendre polynomial
'''
def phiF(l):
    return lambda x : l[x]*f(x)

'''
fun(n) : Approximates f(x) using least square method with legendre polynomial
'''
def fun(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    p = Poly.Polynomial([0])

    a = np.zeros(n+1)
    for i in range(n+1):                                        # least square method using legendre polynomail
        l = legendre(i)
        a[i] = inte.quad(phiF(l), -1, 1)[0]*(2*i + 1)/2         # for legendre ploynomail cj = 2/(2*j + 1)
        p = p + a[i]*legendre(i)                                # a0*L0(x) + a1*L1(x) + a2*L2(x) + ....

    x = np.linspace(-1, 1, 40)                                  #plotting the functions and approximations
    plt.plot(x, f(x), color = 'r', label = "e^x")

    p.show(-1, 1)


#Test cases
# fun(3)