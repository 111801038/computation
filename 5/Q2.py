import Poly
import numpy as np
import scipy.integrate as inte
import matplotlib.pyplot as plt

'''
f(x):   Returns sin(x) + cos(x) ie. the function to be approximated
'''
def f(x):
    return np.sin(x) + np.cos(x)

'''
xf(j) : takes j as input and returns a function of form x^j*f(x) where j is fixed for the function returned
Used for integration in fun() 
'''
def xf(j):
    return lambda x : np.power(x, j) * f(x)

'''
xPow(p) : takes p as input and returns a function of form x^p where x is variable and p is fixed for the function returned
Used for integration in fun()
'''
def xPow(p):
    return lambda x : np.power(x, p)

'''
Appoximates the function sin(x) + cos(x) using a polynomial of degree m provided by the user
Also plots the results and returns the resultant polynomial as an object of type Polynomial
'''

def fun(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    s = np.zeros((n+1, n+1))                            # sotres the integration of function in the matix equation
    b = np.zeros(n+1)                                   # right side of the equation
    for i in range(n+1):                                #forming the matrices by looping through all the elements of matrix
        b[i] = inte.quad(xf(i), 0, np.pi)[0]
        for j in range(n+1):
            s[i, j] = inte.quad(xPow(j+i), 0, np.pi)[0]

    a = np.linalg.solve(s, b)                           # Getting solution of the matix equation

    xs = []                                             #plotting the resultant polynomial and the real function
    ys = []
    for i in np.linspace(0, np.pi):
        xs.append(i)
        ys.append(f(i))

    plt.plot(xs, ys, color = 'r', label = "sin(x) + cos(x)")
    plt.grid()

    p = Poly.Polynomial(list(a))

    p.show(0, 3.14)

    return p


#Test Cases
#print(fun(4))
