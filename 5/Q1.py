import Poly
import numpy as np
import matplotlib.pyplot as plt

'''
fun(points, n): takes poitns and the degree of polynomial to fit as the parameters and
returns the result as object of type Polnomial 
'''
def fun(points, n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
        if not(type(points) == list):
            raise Exception("points must be list of tuples")
        if len(points) == 0:
            raise Exception("points can't be an empty list")
        if not(type(points[0]) == tuple):
            raise Exception("points must be a list of tuples")
    except Exception as e:
        print(type(e))
        print(e)

    s = np.zeros((n+1, n+1))                            # stores the matrix of sum of points
    b = np.zeros(n+1)                                   # right side of the equation
    for i in range(n+1):                                #forming the matrices by looping through all the elements of matrix and points                       
        temp = 0
        for p in points:
            temp += np.power(p[0], i) * p[1]
        b[i] = temp
        for j in range(n+1):
            sum = 0
            for p in points:
                sum += np.power(p[0], (i+j))
            s[i, j] = sum

    a = np.linalg.solve(s, b)                           # Getting solution of the matix equation

    xs = []                                             # plotting the points and returning the result
    ys = []
    minx = points[0][0]                                 # for plotting in the correct range of points
    maxx = points[0][0]
    for p in points:
        xs.append(p[0])
        if p[0] > maxx:
            maxx = p[0]
        if p[0] < minx:
            minx = p[0]
        ys.append(p[1])

    plt.scatter(xs, ys, color = 'r', label = "Points")
    plt.grid()

    p = Poly.Polynomial(list(a))

    p.show(minx - 5, maxx + 5)

    return p

#Test cases
# print(fun([(1, 2), (2, 5), (3,7), (10, 20), (4, 10)], 4))