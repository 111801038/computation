import Poly
import numpy as np
import scipy.integrate as inte

'''
chebyshev(n) : returns nth chebyshev polynomial as a Polynomial object
'''
def chebyshev(n):
    
    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    if n == 0:                                      # T0(x) = 1
        return Poly.Polynomial([1])
    if n == 1:                                      # T1(x) = x
        return Poly.Polynomial([0, 1])

    tn2 = Poly.Polynomial([1])                      # Tn-2
    tn1 = Poly.Polynomial([0, 1])                   # Tn-1
    tn = None                                       # Tn

    for i in range(n-1):                            # Tn = 2*x*Tn-1 - Tn-2
        tn = 2*Poly.Polynomial([0, 1])*tn1 - tn2 
        tn2 = tn1
        tn1 = tn

    return tn

'''
che(i, j) : returns a function g(x) = Ti(x) * Tj(x) * w(x) where Tn(x) is the nth chebyshev polynomial
'''
def che(i, j):
    return lambda x : chebyshev(i)[x]*chebyshev(j)[x]/np.sqrt(1 - np.power(x, 2))

'''
fun() : prints a matrix where element in ith row and jth column = integration of che(i, j) from -1 to 1
ie. for chebyshev polynomails to be orthogonal all non-diagnal entries of the matix must be zero
'''
def fun():

    a = np.zeros((5, 5))    
    for i in range(5):
        for j in range(5):
            a[i, j] = inte.quad(che(i, j), -1, 1)[0]
            print(round(a[i, j], 2), end = ", ")
        print("")

# fun()
