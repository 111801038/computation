import matplotlib.pyplot as plt
import numpy as np

class Polynomial:

    def __init__(self, coeff):                                      #intialize the polynomial with a list of coeff and deg
        
        try:                                                        #raise exception in case its not
            if not(type(coeff) == list):
                raise Exception("Coefficients must be in form of a list")
        except Exception as e:
            print(type(e))
            print(e)

        self.coeff = coeff
        self.deg = len(coeff) - 1

    def __str__(self):                                               # overloading the print
        s = "Coefficient of the polynomial are:\n"
        for i in self.coeff:
            s += str(i) + " "
        return s

    def __add__(self, other):                                       # overloading + operator for adding two polynomials 
        coeff = []

        if self.deg < other.deg:                                    # if degree of first is less than the other one
            return other + self                                     # this is because the below statement for adding assumes self.deg >= other.deg

        for i in range(self.deg + 1):                               
            if i > other.deg:                                       # case when degree of first is greater than deg of second and we ran of degrees
                coeff.append(self.coeff[i])
            else:                                                   # normal case of adding the polynomials
                coeff.append(self.coeff[i] + other.coeff[i])

        
        return Polynomial(coeff)

    def __sub__(self, other):                                       # overloading the subtract operator
        coeff = []

        if self.deg < other.deg:                                    # again the same thing as add below code assumes that deg of first > deg of second
            return (other - self)*(-1)                              # a - b = - (b - a)

        for i in range(self.deg + 1):                                       
            if i > other.deg:                                      # case when degree of first were greater we don't need to substract anything
                coeff.append(self.coeff[i])
            else:                                                   # the normal case of subtracting two polynomials
                coeff.append(self.coeff[i] - other.coeff[i])

        return Polynomial(coeff)

    def __mul__(self, other):                                       # overloading the * operator
        coeff = []
        
        try:
            if type(other) == Polynomial:                               # case when other is a ploynomial
                coeff = [0] * (self.deg + other.deg + 1)
                for i in range(self.deg + 1):
                    for j in range(other.deg + 1):
                        coeff[i+j] += self.coeff[i] * other.coeff[j]    #simply multiplying two coeff and placing them in right index
            elif type(other) == int or type(other) == float:            # case when other is a numeric value
                for i in range(self.deg+1):
                    coeff.append(self.coeff[i] * other)
            else:                                                       # exception case
                raise Exception("* operator requies either a polynomial or numeric value")
        except Exception as e:
            print(type(e))
            print(e)    


        return Polynomial(coeff)

    def __rmul__(self, other):                                           # for cases such as 4 * Polynomial
        return self * other


    def __getitem__(self, x):                                            #overloading the [] return the values of polynomial for a values of x

        res = 0

        for i in range(self.deg + 1):
            res += self.coeff[i]*(x**i)

        return res


    def show(self, start, end):                                           # plots the polynomial

        p = []
        for i in np.arange(start, end, (end-start)/100):
            p.append(self[i])

        plt.plot(np.arange(start, end, (end-start)/100), p, label = "Polynomial")
        plt.ylabel("f(x)")
        plt.xlabel("x")
        plt.legend()
        plt.show()
        

    def fitViaMatrixMethod(self, points):                                   # finds the best fir polynomail for given points using matrix method
        
        deg = len(points)

        mat = np.zeros((deg, deg))
        b = np.zeros(deg)

        for i in range(deg):
            for k in range(deg):
                mat[i, k] = np.power(points[i][0], k)                       # generating the matrix for ploynomial equations
            b[i] = points[i][1]

        x = np.linalg.solve(mat, b)                                         # using lingalg for solving the matrix equation

        x = list(x)

        min = points[0][0]                                                  # min and max on x asix for constraints on plot
        max = points[0][0]
        for k in points:
            plt.scatter(k[0], k[1], color = 'r')                            #plotting the given points
            plt.title("Matix method")
            if k[0] > max:
                max = k[0]
            if k[0] < min:
                min = k[0]

        x = Polynomial(x)

        x.show(min - 5, max + 5)                                            # plotting the polynomial

    def fitViaLagrangePoly(self, points):                                   # finds the best fit polynomial using lagrange method
        
        deg = len(points)

        psi = Polynomial([0])
        for j in range(deg):
            psiJ = Polynomial([1])
            for i in range(deg):
                if i == j:                                                  # skip case when i == j
                    continue
                x = Polynomial([-points[i][0], 1])                          # (x - xi)
                x = x*(1/(points[j][0] - points[i][0]))                     # (x - xi)/(xj - xi)
                psiJ = psiJ * x                                             # product of (x - xi)/(xj - xi) for all i
            psi = psi + points[j][1] * psiJ                                 # sum of all psiJ

        min = points[0][0]                                                  # similar to matrix method function
        max = points[0][0]
        for k in points:
            plt.scatter(k[0], k[1], color = 'r')
            plt.title("Lagrange Method")
            if k[0] > max:
                max = k[0]
            if k[0] < min:
                min = k[0]

        psi.show(min - 5, max + 5)

    def derivative(self):                                                   # returns derivative of a polynomial
        
        coeff = []

        for i in range(1, self.deg + 1):                                    # ie. ax^2 + bx + c -> 2ax + b
            coeff.append(i* self.coeff[i])                                  # calculating coefficients of derivative oif polynomial

        return Polynomial(coeff)


    def area(self, start, stop):                                            # return area under polynomial from start to end

        try:
            if not(type(start) == int or type(start) == float):
                raise Exception("starting values must be numeric")
            if not(type(stop) == int or type(stop) == float):
                raise Exception("Ending value must be numeric")
        except Exception as e:
            print(type(e))
            print(e)

        coeff = [0]

        for i in range(self.deg + 1):                                       # ie. ax + b -> (a/2)x^2 + bx + 0
            coeff.append(self.coeff[i]/(i+1))                               # calculating coefficients of integral of polynomial

        p = Polynomial(coeff)

        return "Area in the interval [{0}, {1}] is : {2}".format(start, stop, p[stop] - p[start])                                           # g(stop) - g(start) where g is integration of f
            
# Test Cases
# p = Polynomial([1,2, 3])
# print(p.derivative())
# print(p.area(0,10))
# p = Polynomial([1,2,3])
# p2 = Polynomial([1, 1, 1])
# print(p + p2)
# print(p[2])
# p.show(-10, 10)
# Polynomial([]).fitViaLagrangePoly([[1,1], [2,3], [3,4], [10, 11]])
# Polynomial([]).fitViaMatrixMethod([[1,2], [2,3], [3,5], [4,10]])