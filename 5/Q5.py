import Poly
import numpy as np

'''
chebyshev(n) : returns nth chebyshev polynomial as a Polynomial object
'''
def chebyshev(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    if n == 0:                                      # T0(x) = 1
        return Poly.Polynomial([1])
    if n == 1:                                      # T1(x) = x
        return Poly.Polynomial([0, 1])

    tn2 = Poly.Polynomial([1])                      # Tn-2
    tn1 = Poly.Polynomial([0, 1])                   # Tn-1
    tn = None                                       # Tn

    for i in range(n-1):                            # Tn = 2*x*Tn-1 - Tn-2
        tn = 2*Poly.Polynomial([0, 1])*tn1 - tn2 
        tn2 = tn1
        tn1 = tn

    return tn

#Test Cases
# print(chebyshev(0))
# print(chebyshev(1))
# print(chebyshev(2))
# print(chebyshev(3))
# print(chebyshev(4))
