import numpy as np
import scipy.integrate as inte
import matplotlib.pyplot as plt

'''
f(x) : returns e^x
'''
def f(x):
    return np.power(np.e, x)

'''
fcos(k) : returns a function g(x) = f(x)*cos(kx)
'''
def fcos(k):
    return lambda x : np.cos(k*x)*f(x)

'''
fsin(x) : returns a function h(x) = f(x)*sin(kx)
'''
def fsin(k):
    return lambda x : np.sin(k*x)*f(x)

'''
fun(n) : aprroximated a funtion f(x) using fourier series and prints and plots the approximation 
'''
def fun(n):

    try:                                                #Exception handling
        if not(type(n) == int):
            raise Exception("n must be an integer")
    except Exception as e:
        print(type(e))
        print(e)

    a0 = 0                                                  #coeffcients for the the fourier series
    a = np.zeros(n)
    b = np.zeros(n)

    a0 = inte.quad(fcos(0), -np.pi, np.pi)[0]/np.pi         #calulating a0

    print("a0 = {0}".format(a0))

    for i in range(n):                                      #caculation aj and bj where j belong to {1, 2, .... n}
        a[i] = inte.quad(fcos(i+1), -np.pi, np.pi)[0]/np.pi
        b[i] = inte.quad(fsin(i+1), -np.pi, np.pi)[0]/np.pi
        print("a{0} = {1}".format(i+1, a[i]))
        print("b{0} = {1}".format(i+1, b[i]))

    xs = np.linspace(-np.pi, np.pi, 100)                    #sampling points to plot the graph
    ys = []

    for x in xs:                                            #getting the fourier approximation for different value of x
        sum = a0/2
        for k in range(n):
            sum += a[k]*np.cos((k+1)*x) + b[k]*np.sin((k+1)*x)
        ys.append(sum)
                                                            #plotting the graph
    plt.plot(xs, ys, color = 'r', label = "Fourier Approximation")
    plt.plot(xs, f(xs), label = "e^x")
    plt.xlabel("x")
    plt.ylabel("f(x)")
    plt.legend()
    plt.grid()
    plt.show()

#Test cases
fun(5)