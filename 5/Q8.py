from scipy.fft import fft, ifft
import numpy as np
import Poly
from timeit import default_timer as timer
import matplotlib.pyplot as plt

'''
strToList(s) : converts the string s to list of int
'''
def strToList(s):
    l = []
    for i in range(len(s)-1, -1, -1):
        l.append(int(s[i]))
    q = []
    for i in range(0, len(l), 13):
        num = 0
        j = 12
        if j + i >= len(l):
            j = len(l) - i - 1
        while j >= 0:
            num = num*10 + l[i+j]
            j -= 1
        q.append(num)
    return q

'''
fun(a, b, p) : a and b are the large numbers to be multiplied and are taken as input in form of a string
prints the multiplication of two large numbers using two methods
P : default 0, set it to 1 to print the results also (Use it only for digits < 10 )
1) Using Polynomial class implemented in previous assignment and
2) Using scipy.fft module 
'''
def fun(a, b, p = 0):

    a = strToList(a)                            # converting the numbers from string to list of int
    b = strToList(b)
 
    sizeA = len(a)                              # getting the sizes of both the numbers
    sizeB = len(b)

    aP = Poly.Polynomial(a)                     # Using polynomail method to get the result
    bP = Poly.Polynomial(b)
    start = timer()
    cP = aP * bP
    end = timer()

    if p == 1:
        print("Polynomial result = ", cP[10], " Time taken = ", (end - start))

    pTime = end - start
    # pTime = 0

    start = timer()
    c = np.real( ifft( fft(a, sizeA + sizeB) * fft(b, sizeA + sizeB) ) )      # Using scipy.fft to get the result
    end = timer()

    fTime = end - start

    if p == 1:        
        res = 0
        for i in range(len(c)):
            res += (10**i)*c[i]

        print("scipy.fft result = ", res, " Time taken = ", (end - start))

    return (pTime, fTime)

'''
f() : plots the time taken by polynomial and fft method for numbers of legth from 1 to 1000
'''
def f():
    num = 100                                       #number of points to sample
    t = 100                                         #number of iterations
    r = 1000                                        #limit of number of digits
    
    xs = np.linspace(1, r, num)   #sampling points     
    ysP = np.zeros(num)
    ysF = np.zeros(num)

    for k in range(t):                              #running iterations
        ysP_ = np.zeros(num)
        ysF_ = np.zeros(num)
        for i in range(len(xs)):
            res = fun("1"*(int(xs[i])), "2"*(int(xs[i])))
            ysP_[i] = res[0]
            ysF_[i] = res[1]
        ysP += ysP_
        ysF += ysF_

    ysP = ysP/t                                     #taking average of each iteration
    ysF = ysF/t    
                                                    #plotting the graphs
    plt.plot(xs, ysP, label = "Time taken by Polynomial method") 
    plt.plot(xs, ysF, label = "Time taken by Fourier method")
    plt.xlabel("n")
    plt.ylabel("time")
    plt.legend()
    plt.grid()
    plt.show()

# f()
# fun("123", "123", p = 1)