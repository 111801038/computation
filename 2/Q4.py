import Q2
import numpy as np
import matplotlib.pyplot as plt


def connectedComponent():

    pList = np.linspace(0, 0.008, 50)                       #vales of p between 0 and 0.008

    avgMaxComp = []                                         #average size of largest component for a specific value of p
    avgSecMaxComp = []                                      #average size of second largest component for a specific value of p

    turns = 50                                              #number of samples to take the average of

    for p in pList:
        maxComp = 0                                         #sum of actual size of largest component
        secMaxComp = 0                                      #sum of actual size of second largest componenet
        for i in range(turns):
            g = Q2.ERRandomGraph(1000)                      #generating a random graph
            g.sample(p)
            ret = g.oneTwoComponentSizes()                  #getting the size of largest and second largest components
            
            maxComp +=ret[0]/1000                           #adding the returned value after normalization
            secMaxComp += ret[1]/1000

        avgMaxComp.append(maxComp/turns)                    #dividind by turns to get average of values
        avgSecMaxComp.append(secMaxComp/turns)

                                                            #plotting the graph
    plt.plot(pList, avgMaxComp, color = 'g', label = "Largest Connected Component")
    plt.plot(pList, avgSecMaxComp, color = 'b', label = "2nd largest connected component")
    plt.axvline(x = 0.001, color = 'r', label = "Largest CC threshold")
    plt.axvline(x = np.log(1000)/1000, color = 'y', label = "Connectedness threshold")
    plt.ylabel("fraction of nodes")
    plt.xlabel("p")
    plt.title("Fraction of nodes in the largest and second-largest connected components (CC) of G(1000, p) as function of p")
    plt.legend()
    plt.ylim(0,1)
    plt.grid()
    plt.show()

#Test Cases

# g = Q2.ERRandomGraph(100)
# g.sample(0.01)
# print(g.oneTwoComponentSizes())

connectedComponent()