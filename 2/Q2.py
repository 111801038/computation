import matplotlib.pyplot as plt
import Q1
import numpy as np

class ERRandomGraph(Q1.UndirectedGraph):

    def sample(self, p):
        try:
            if self.vertex == None:                     #samples onty non-free graphs
                raise Exception("Graph must not be a free graph")
        except Exception as e:
            print(type(e))
            print(e)

        for i in range(self.vertex):                #iterate through all the vertices                
            j = i + 1
            while(j < self.vertex):                 #forms a pair with i and en edge is placed between them based on value of p
                
                r = np.random.rand()

                if r <= p:                          #adds an edge between two nodes based on
                    self = self + (i + 1, j + 1)    # value of a randomly generated number 'p'
                
                j += 1

        return self                                 #return the new graph


# g = ERRandomGraph(1000)
# g.sample(0.4)
# g.plotDegDist()