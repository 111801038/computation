import Q5
import numpy as np
import matplotlib.pyplot as plt

def percolation():

    pList = np.linspace(0, 1, 50)                       #list of values of p between 0 and 1
    avgCount = []                                       #fraction of end-end percolations occurred out of 'turns'
    turns = 50                                          #number of times to run the code for each value of p

    for p in pList:
        count = 0         
        for i in range(turns):
            l = Q5.Lattice(100)                         #generate a lattice
            l.percolate(p)                              #percolate it
            ret = l.existsTopDownPath()                 #check if end-end percolation exists or not
            if ret:                                     #if yes then inc the count
                count += 1
        avgCount.append(count/turns)                    #fration of times ret was true for a value of p

    plt.plot(pList, avgCount)                           #plotting the graphs
    plt.ylabel("fraction of runs ene-end percolation occurred")
    plt.xlabel("p")
    plt.title("Critical cut-off in 2-D bond percolation")
    plt.grid()
    plt.show()

percolation()