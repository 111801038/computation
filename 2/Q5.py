import networkx as nx
import matplotlib.pyplot as plt
from networkx.algorithms.shortest_paths.generic import shortest_path
import numpy as np

class Lattice:

    def __init__(self, n):                                      #constructor for the class

        self.g = nx.empty_graph(0)                              #generating an empty graph and then adding the noes manually
        rows = range(n)
        self.g.add_nodes_from((i, j) for i in rows for j in rows) #adds nodes to the graph, nodes have labels of form (x,y) 
        self.n = n                                              #where x and y give their x and y give their position in lattice        


    def show(self):                                             #plots the grid
        pos = {(x,y):(y,-x) for x,y in self.g.nodes()}          #positions the nodes as per the standard given in pdf
        nx.draw_networkx(self.g, pos = pos, width = 2, node_size = 1, with_labels= False, edge_color = 'r')  #drqws the grid
        plt.axis("off")
        plt.show()


    def percolate(self, p):                                     #simulates percolation on the lattice

        for i in range(self.n):                                 #iterate through the (i,j) nodes
            for j in range(self.n):

                if i < self.n - 1 :                             #this adds edge to the bottom of the node
                    r = np.random.rand()
                    if r <= p:
                        self.g.add_edge((i+1, j), (i, j))

                if j < self.n - 1 :                             #this adds edge to the right of the node, this avoids repeatition
                    r = np.random.rand()
                    if r <= p:
                        self.g.add_edge((i, j+1), (i, j))


    def existsTopDownPath(self):                                #returns if there exists a path from top row to the 
                                                                #bottom row of lattice
        for i in range(self.n):
            for j in range(self.n):                                 #iterate through the nodes
                if nx.has_path(self.g, (0, i), (self.n - 1, j)):    #has_path is an inbuilt function, check if there is a path between                
                    return True                                     #(0,i) and (n-1,j) .ie top most layer and bottomost layer

        return False


    def showPaths(self):                                            #displays the shortest paths .......
        
        for e in self.g.edges():                                    #set edge colour for all edges to red
            self.g[e[0]][e[1]]['color'] = 'r'
                    
        for i in range(self.n):                                     #for each element i of top layer
            
            shortPath = []                                          #shortest path found
            hasPath = False                                         #if a particular node even has a path or not

            for j in range(self.n):
                
                if nx.has_path(self.g, (0, i), (self.n-1, j)):      #case 1 when there exists a path from i to the bottom layer
                    hasPath = True
                    path = nx.shortest_path(self.g, (0, i), (self.n - 1, j))    #returns a path between i of top layer
                                                                                # and j of bottom layer
                    if shortPath == []:                             #in case no other path has been found for i this is the shortest path
                        shortPath = path
                    
                    if len(list(shortPath)) > len(list(path)):      #if we find a better path from i to bottom layer
                        shortPath = path

            if not hasPath:                                         #case 2 when there is not path from i to bottom layer
                                                                    #display the largest shortest path from i
                paths = nx.shortest_path(self.g, (0, i))            #all possible shortest paths from i to all other nodes
                max = 0                                             #length of the largest shortest path from i
           
                for path in paths.values():                         #iterate throught all the paths

                    if len(path) > max:                             #find the largest one
                        shortPath = path
                        max = len(path)
           
            for k in range(len(shortPath)-1):                       #colour all edges that in the shortest path as green
                self.g[shortPath[k]][shortPath[k+1]]['color'] = 'g'
        
                                                                    #plotting the graphs
        edge_color_list = [self.g[e[0]][e[1]]['color'] for e in self.g.edges() ]    #list of colour for each edge
        pos = {(x,y):(y,-x) for x,y in self.g.nodes()}
        nx.draw(self.g, width = 2, edge_color = edge_color_list, pos = pos, node_size = 1,  with_labels = False)
        plt.show()



#Test Cases

# l = Lattice(25)
# l.percolate(0.4)
# print(l.existsTopDownPath())
# l.showPaths()
# l.show()

# l = Lattice(25)
# l.percolate(0.6)
# print(l.existsTopDownPath())
# l.showPaths()
# l.show()



