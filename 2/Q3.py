import Q2
import numpy as np
import matplotlib.pyplot as plt

def Connectivity():
    
    pList = np.linspace(0, 0.1, 50)                     #values of p between 0 and 0.1

    avgCount = []                                       #avg count from 1000 runs

    for p in pList:
        count = 0                                       #actual count of 1000 runs
        for i in range(1000):
            g = Q2.ERRandomGraph(100)
            g = g.sample(p)
            if(g.isConnected()):
                count += 1
        avgCount.append(count/1000)                     #taking the average count for that value of p

                                                        #plotting the graphs
    plt.axvline( x = np.log(100)/100, color = 'r', label = " Theoretical threshold")
    plt.plot(pList, avgCount)
    plt.grid()
    plt.ylabel("fraction of runs G(100,p) is connected")
    plt.xlabel("p")
    plt.title("Connectedness of of a G(100,p) as function of p")
    plt.legend()
    plt.show()


Connectivity()