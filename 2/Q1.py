import matplotlib.pyplot as plt
import numpy as np

class UndirectedGraph:

    def __init__(self, vertex = None):                      #constructor for UndirectedGraph()
        
        self.vertex = vertex                                #number of vertex
        self.l = {}                                         #graph in adjacency list format
        self.nodes = 0                                      #number of nodes in graph will be different from vertex in case of free graphs
        self.edges = 0                                      #number of edges
        
        if not(vertex == None):                             #in case of non free graph
            for i in range(vertex):
                self.l[i+1] = []                                     


    def addNode(self, node):                                #add a node to the graph
        try:
            if not(isinstance(node, int)):                      #check if given node is an integer of not
                raise Exception("Node should be a positive integer")
    
            if not(self.vertex == None):                        #check if graph is a free graph or not
                if node > self.vertex:
                    raise Exception("Node index cannot exceed number of nodes")
        
            if node <= 0:                                       #check if node given is positive or not
                raise Exception("Node should be a positive integer")

            if node not in self.l.keys():                       #if node does not exists create it else raise exception
                self.l[node] = []
                self.nodes += 1
            else:
                raise Exception("Node already exists")
        except Exception as e:
            print(type(e))
            print(e)

    def addEdge(self, node1, node2):                        #add edges to the graph
        
        if node1 not in self.l.keys():                      #check if given nodes alreasy exits or not
            self.addNode(node1)                             #if not then add them

        if node2 not in self.l.keys():
            self.addNode(node2)

        if node2 in self.l[node1]:
            return -1

        self.l[node1].append(node2)                         #add the edge to the adjacency list
        self.l[node2].append(node1)
        self.edges += 1

        return 1


    def plotDegDist(self):
        
        vertex = self.vertex
        if self.vertex == None:                             #if its a free graph then number of nodes will be considered as equal to
            vertex = self.nodes                             #number of vertex
        
        deg = np.zeros(vertex)                              #store the count of each degree type 

        for x in self.l.keys():                             #inc the corresponding value in deg given the degree of current node 
            deg[len(self.l[x])] += 1
        
        avg = 0                                             #average of the degrees

        for i in range(vertex):                             #calculating the average
            avg += deg[i]*i
        
        avg = avg/vertex
        
        s = 25                                              #size of points for graph

        if(vertex > 100):                                   #in case no. of points is large shift to a smaller size
            s = 5
        
        deg = deg/vertex
        plt.grid()
        plt.axvline(avg, color = 'r', label = "Avg. node degree")              #plotting the graphs
        plt.scatter(range(vertex), deg, color = 'b' , label = 'Actual degree distribution', s = s)
        plt.ylabel("Fraction of nodes")
        plt.xlabel("Node degree")
        plt.legend()
        plt.show()

    def __add__(self, other):                               #overload the "+" operation
        
        if type(other) == tuple:                            #if its a tuple then add edge otherwise add the node
            self.addEdge(other[0], other[1])
        else:
            self.addNode(other)

        return self
    
    def __str__(self):                                      #overload the print function
        
        str = "Graph with {0} nodes and {1} edges. Neighbours of the nodes are below:".format(self.nodes, self.edges)
        for x in self.l.keys():
            str += '\n' + "Node {0}: {1}".format(x, self.l[x])
        return str

    def bfs(self, visited, key):                            #iterate the graph using BFS starting at a node 'key'

        que = []        
        que.append(key)

        while(True):                                        #only visits the nodes that are reachable from node 'key'            
            x = que.pop(0)                                  #dequeue from the queue

            if visited[x] == 0:
                visited[x] = 1
            
            for y in self.l[x]:                             #visiting the neighbours
                if visited[y] == 0:
                    que.append(y)                           #enque to the queue
                    visited[y] = 1
            
            if len(que) == 0:                               #if queue is empty that means there are no nodes left to explore
                break
        
        return visited                                      #return a dict with nodes as keys, with 1 as value if it was visited
                                                            #else 0           

    def isConnected(self):                                  #start at any node and do a bfs, if all nodes are visited in just
                                                            #one call of function bfs() then it's connected
        vertex = self.vertex
        visited = {}                                        #dictionary that stores the visited status of each node
        
        if vertex == None:
            vertex = self.nodes

        for x in self.l.keys():
            visited[x] = 0

        if len(self.l.keys()) == 0:
            return False

        visited = self.bfs(visited, list(self.l.keys())[0]) #bfs with 'key' as the first node in graph

        for q in visited.values():                          #if after bfs there are still some nodes that are unvisited
            if q == 0:                                      #then graph is not connected
                return False

        return True                                         #else graph is connected


    def oneTwoComponentSizes(self):                         #returns size of largest 2 componenets in a graph

        vertex = self.vertex
        visited = {}
        maxComp = 0
        secMaxcomp = 0

        if vertex == None:
            vertex = self.nodes
        
        for x in self.l.keys():
            visited[x] = 0

        compSize = 0                                        #size of current component        

        for q in self.l.keys():                             #do a bfs and subtract the no. of ndoes that were visited previously
                                                            #from no. of nodes that have been visited now
            if(visited[q] == 1 ):                           
                 continue                                             
                                                            #since this implementation of bfs only traverses the nodes reachable 
            prev = sum(list(visited.values()))              #from starting node, this means that at any call bfs will travese the entire
            visited = self.bfs(visited, q)                  #componenet hence the number of ndoes visited in the current bfs step
            compSize = sum(list(visited.values())) - prev   #will give the size of the componenet
            
            if maxComp == compSize:                         #this case is when there are two components of size to max size
                secMaxcomp = compSize
            elif compSize > maxComp:                        #if the new component is larger than previous largest
                secMaxcomp = maxComp
                maxComp = compSize
            elif compSize > secMaxcomp and compSize < maxComp:      #second largest component
                secMaxcomp = compSize

        return [maxComp, secMaxcomp]


#Test Cases

# g = UndirectedGraph(5)
# g = g + (1, 2)
# g = g + (2, 3)
# g = g + (3, 4)
# g = g + (3, 5)
# print(g)
# g.plotDegDist()

# g = UndirectedGraph()
# g = g + (1, 2)
# g = g + (2, 3)
# g = g + (3, 4)
# print(g)
# g.plotDegDist()

# g = UndirectedGraph(5)
# g = g + (1, 2)
# g = g + (2, 3)
# g = g + (3, 4)
# g = g + (3, 5)
# print(g.isConnected())

# g = UndirectedGraph(5)
# g = g + (1, 2)
# g = g + (2, 3)
# g = g + (3, 5)
# print(g.isConnected())

# g = UndirectedGraph(6)
# g = g + (1, 2)
# g = g + (3, 4)
# g = g + (6, 4)
# print(g.oneTwoComponentSizes())