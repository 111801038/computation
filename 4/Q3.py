import numpy as np
import matplotlib.pyplot as plt


def sind(x):                                    #returns derivative the of sin(x^2) ie. 2x.cos(x^2)
    return np.cos(x**2)*(2*x)

def sin(x):                                     #returns sin(x^2)
    return np.sin(x**2)

def sin2d(x):
    return 2*np.cos(x**2) - np.sin(x**2)*4*(x**2)

def sin3d(x):
    return -12*x*np.sin(x**2) - 8*(x**3)*np.cos(x**2)

def fun():
    
    hCfd = []
    hFfd = []
    xCfd = []
    xFfd = []

    hlist = np.linspace(0.0000001, 0.1, 1000)
    x = np.linspace(0, 1)                 #sample equidistant points

    for h in hlist:

        f_ = sind(x)                                #getting the actual derivative

        ffd = []                                    #forward finite derivative
        cfd = []                                    #central derivative

        for i in range(len(x)):                 #calculating the derivatives
            ffd.append( ( sin(x[i] + h) - sin(x[i]) ) / h  )
            cfd.append( ( sin(x[i] + h) - sin(x[i] - h) ) / (2*h) )

        errFfd = []                                 #error for forward finite derivative 
        errCfd = []                                 #error for central finite derivative

        for i in range(len(ffd)):                   #calculating the absolute errors
            errFfd.append( abs( ffd[i] - f_[i] ) )
            errCfd.append( abs( cfd[i] - f_[i] ) )

        hCfd.append(max(errCfd))
        hFfd.append(max(errFfd))
        xCfd.append(np.argmax(errCfd))
        xFfd.append(np.argmax(errFfd))

    plt.plot( hlist, hFfd, label = "Forward Finite Dertivative")     #plotting the errors
    plt.plot( hlist, hCfd, label = "Central Finite Derivative")

    plt.plot(hlist, abs(hlist*sin2d(x[xFfd])/2), label = "Foward Finite Theoretical")
    plt.plot(hlist, abs((hlist**2)*sin3d(x[xCfd] + hlist)/6), label = "Central Finite Theoretical")

    plt.ylabel("error")
    plt.xlabel("h")
    plt.grid()
    plt.legend()
    plt.show()


fun()