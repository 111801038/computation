import Q6
import numpy as np
import scipy.integrate as inte

def f(x):                                                   #return e^x.sin(x)
    return np.power(np.e, x)*np.sin(x)


def fun():
    
    p = Q6.Polynomial([0, 1, 1, 1/3, 0, -1/30, -1/90 ])         #taylor series expansion for e^x.sin(x) till 5th derivative

    pArea = p.area(0, 0.5)

    print(pArea)                                                                #using the taylor's approximation to find the area 
    
    sciArea = inte.quad(f, 0, 0.5)                                              #Area using the scipy.integrate

    print("Area using scipy.integrate = {0}".format(sciArea[0]))

    pArea = float(str.split(pArea, ":")[1])

    print("error = {0}".format(abs(pArea - sciArea[0]) + sciArea[1]) )          # error = error of scipy.integrate + divergence from scipy.Area of polnomial method

fun()