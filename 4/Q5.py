import scipy.integrate as inte
import numpy as  np
import matplotlib.pyplot as plt



def fun():
    
    
    xlist = np.linspace(0.0001, 3, 100)                     #sampling points bewteen (0,3)

    romb = []                                               #result from intergrate.romberg
    trapz = []                                              #result from integrate.trapezoid
    simps = []                                              #result from integrate.simpson

    for x in xlist:


        f = lambda x: 2*x*np.power(np.e, x**2)              #defining 2x.e^(x^2) as a lambda function
        
        points = np.arange(0, x, 0.03)                      #sample points between (0, x)

        romb.append(inte.romberg(f, 0, x))                  #getting the results

        trapz.append(inte.trapezoid(f(points), points))

        simps.append(inte.simpson(f(points), dx =0.01))

    plt.plot(xlist, trapz, label = "Trapezoidal")           #plotting the results
    plt.plot(xlist, simps, label = "Simpson")
    plt.plot(xlist, romb, label = "Romberg integration")
    plt.plot(xlist, np.power(np.e, xlist**2) - 1, label = "True area" )
    plt.xlabel("x")
    plt.ylabel("Area")
    plt.legend()
    plt.grid()
    plt.show()



fun()
