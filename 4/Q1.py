import numpy as np
import matplotlib.pyplot as plt


def sind(x):                                    #return derivative of sin(x^2) ie, 2x.cos(x^2)
    return np.cos(x**2)*(2*x)

def sin(x):                                     #return sin(x^2)
    return np.sin(x**2)


def fun():

    x = np.arange(0, 1.01, 0.01)                #sample equidistant points between [0,1] including both 0 and 1

    f_ = sind(x[:100])                          #getting actual derivative 

    ffd = []                                    #forward finite derivative                                    

    for i in range(len(x) - 1):                 #calculate the derivative using forward finite method
        ffd.append( (sin(x[i+1]) - sin(x[i]))/0.01  )

    plt.plot(x[:100], f_, label = "Actual Derivative")          #plotting the values
    plt.plot(x[:100], ffd, label = "Forward finite difference")

    plt.ylabel("f '(x)")
    plt.xlabel("x")
    plt.legend()
    plt.show()



fun()
