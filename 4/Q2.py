import numpy as np
import matplotlib.pyplot as plt


def sind(x):                                    #returns derivative the of sin(x^2) ie. 2x.cos(x^2)
    return np.cos(x**2)*(2*x)

def sin(x):                                     #returns sin(x^2)
    return np.sin(x**2)


def fun():

    x = np.arange(-0.01, 1.01, 0.01)            #sample equidistant points

    f_ = sind(x)                                #getting the actual derivative

    ffd = []                                    #forward finite derivative
    bfd = []                                    #backward finite derivative
    cfd = []                                    #central derivative

    for i in range(len(x) - 2):                 #calculating the derivatives
        ffd.append( ( sin(x[i+2]) - sin(x[i+1]) ) / 0.01  )
        bfd.append( ( sin(x[i+1]) - sin(x[i]) ) / 0.01 )
        cfd.append( ( sin(x[i+2]) - sin(x[i]) ) / 0.02 )

    errFfd = []                                 #error for forward finite derivative 
    errBfd = []                                 #error for backward finite derivative
    errCfd = []                                 #error for central finite derivative

    for i in range(len(ffd)):                   #calculating the absolute errors
        errFfd.append( abs( ffd[i] - f_[i+1] ) )
        errBfd.append( abs( bfd[i] - f_[i+1] ) )
        errCfd.append( abs( cfd[i] - f_[i+1] ) )

    plt.plot( x[1 : 101], errFfd, label = "Forward Finite Dertivative")     #plotting the errors
    plt.plot( x[1 : 101], errBfd, label = "Backward Finite Derivative")
    plt.plot( x[1 : 101], errCfd, label = "Central Finite Derivative")

    plt.ylabel("error")
    plt.xlabel("x")
    plt.legend()
    plt.show()



fun()
