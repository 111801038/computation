import numpy as np
import matplotlib.pyplot as plt

def y(x):                                           #returns 2x.e^(x^2)
    return 2*x*np.power(np.e, x**2)         

def fun():
    
    inte = []
    
    mlist = np.arange(1, 30)                        #sampling m between 1 to 30

    for m in mlist:

        x = np.linspace(1, 3, m)                    #sampling x from (1, 3)

        it = 0

        for i in range(len(x) - 1):                 #apply the trapezoidal method
            it += y(x[i]) + y(x[i+1])

        it = it/m

        inte.append(it)                             #storing the integration in list

    plt.axhline(y = (np.e**9) - np.e, color='r', label ='True area')        #plotting the graphs
    plt.plot(mlist, inte, label = "Trapeziodal method")
    plt.xlabel("M")
    plt.ylabel("Area")
    plt.grid()
    plt.legend()
    plt.show()

fun()




