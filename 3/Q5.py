import scipy.interpolate as inter
import matplotlib.animation as anim
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()  

def fun(t):                                                                 # the function to approximate
    return np.tan(t) * np.sin(30 * t) * np.power(np.e, t)


def animate(i): 


    plt.clf()                                                               # clear the previous plot
    
    plt.title("Difference interpolation of tan(x).sin(30x).e^x for {0} samples".format(i+2))

    x = np.linspace(0.0001, 1, i + 2)                                       # generating values in the the interval
    y = []
    Q = np.arange(0, 1, 0.001)
    
    for t in Q:                                                             # plotting the true fucntion
        y.append(fun(t))
    plt.plot( Q, y, color = 'r', label = "True")

    x_ = (fun(x))
    
    x_ = np.array(x_)

    cubic = inter.CubicSpline(x, x_)                                        # generating a cubic approx and plotting it

    plt.plot(Q, cubic(Q), color = 'b', label = "Cubic")

    bary = inter.BarycentricInterpolator(x, x_)                             # generating a Barycentric approximation and plotting it

    plt.plot(Q, bary(Q), color = 'g', label = "Barycentric")

    akima = inter.Akima1DInterpolator(x, x_)                                # generating a akima approximation and plotting it

    plt.plot(Q, akima(Q), color = 'purple', label = "Akima")

    plt.ylim(-4, 4)
    plt.xlim(0, 1)
    plt.legend(loc = "upper left")
    plt.ylabel("f(x)")
    plt.xlabel("x")

a = anim.FuncAnimation(fig, animate, frames = 35, interval = 500)           # creates the animation 

# plt.show()

Writer = anim.writers['ffmpeg']                                             # for saving the animation onto the machine
writer = Writer(fps=15, metadata=dict(artist='Me'), bitrate=1800)

a.save('Q5.mp4', writer=writer)

# anim.save('growingCoil.mp4', writer = 'pillow', fps = 30) 
