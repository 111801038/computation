class RowVectorFloat:

    def __init__(self, vec):
        
        try:                                                            #handling Exceptions
            for i in vec:
                if not(type(i) == int or type(i) == float):
                    raise Exception("Only numeric values are allowed")
        except Exception as e:
            print(type(e))
            print(e)
            return

        self.vec = vec                                                  #row vector

    def __str__(self):                                                  # for overloading print    
        
        s = ""
        
        for i in self.vec:
            s = s + '%.2f'%i + " "
        
        return s

    def __len__(self):                                                  # overloading len on RowVectorFloat
        return len(self.vec)

    def __getitem__(self, n):                                           #overloading [] 
                                                                        #return element at index n
        try:
            if(len(self) <= n):
                raise Exception("Out of index")
        except Exception as e:
            print(type(e))
            print(e)
            return
        
        return self.vec[n]

    def __setitem__(self, n, v):                                        #sets items at index n to v

        try:
            if not(type(v) == int or type(v) == float):
                raise Exception("Only numeric values are allowed")
        except Exception as e:
            print(type(e))
            print(e)
            return

        self.vec[n] = v

    def __mul__(self, other):                                           #overloads * operator for numeric types

        try:
            if not(type(other) == int or type(other) == float):
                raise Exception("Invalid expression")
        except Exception as e:
            print(type(e))
            print(e)
            return

        l = []
        for i in range(len(self.vec)):
            l.append(other*self.vec[i])

        return RowVectorFloat(l)

    def __rmul__(self, other):                                          #for the case when numeric value is on left of expression
                                                                        # in multiplication ie. n * RowVectorFloat()
        try:
            if not(type(other) == int or type(other) == float):
                raise Exception("Invalid expression")
        except Exception as e:
            print(type(e))
            print(e)
            return

        l = []
        for i in range(len(self.vec)):
            l.append(other*self.vec[i])

        return RowVectorFloat(l)


    def __add__(self, other):                                           #overloads the + operator on for two RowVectorFloats

        try:
            if not(isinstance(other, RowVectorFloat)):
                raise Exception("Invalid Expression")
        except Exception as e:
            print(type(e))
            print(e)
            return

        l = []
        for i in range(len(self)):
            l.append(self.vec[i] + other.vec[i])
        
        return RowVectorFloat(l)

#Test Cases

# r1 = RowVectorFloat([1, 2 ,3])
# r2 = RowVectorFloat([4, 5, 6])
# print(r1 + r2)
# print(2*r1)
# print(2*r1 + 0.5*r2)
# r1[2] = 5
# print(r1)


