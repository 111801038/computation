import Q2
import matplotlib.pyplot as plt


def convergence(b, n, m):

    q = True                                                            

    mat = Q2.SquareMatrixFloat(n)
    while q:                                                            # keep on genetaing a matrix until we get a diagonally dominant one
        mat.sampleSymmetric()                                           # so as to apply Jacobi method and GS method
        q = not(mat.isDRDominant())

    (e, x) = mat.jSolve(b, m)                                           # calls jacobi method and plots the error

    plt.plot(range(1, m+1), e , color = 'r' ,label = "Jacobi method")

    (e2, x2) = mat.gsSolve(b, m)                                        # similarly for GS method

    plt.plot(range(1, m+1), e2, color = 'b', label = "GS method")       

    plt.xlabel("number of iterations")
    plt.ylabel("error")
    plt.legend()
    plt.show()

# convergence([101231230, 12312312, 021321312312.4], 3, 20)

