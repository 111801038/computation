import Q1
import numpy as np
import random
import copy

class SquareMatrixFloat:

    def __init__(self, n):                                                      #constructor for SquareMatrixFloat

        try:                                                                    #handling exceptions
            if not(type(n) == int):
                raise Exception("Dimesions must be integer")
        except Exception as e:
            print(type(e))
            print(e)

        self.n = n
        self.mat = []        

        for i in range(n):                                                      #object consists of list of n RowVectorFloat
            self.mat.append(Q1.RowVectorFloat([0]*n))

    def __str__(self):                                                          #overloading print 
        s = "The matrix is:\n"

        for i in self.mat:
            s = s + str(i) + "\n"
        s = s.rstrip("\n")
        return s

    def sampleSymmetric(self):                                                  #create a randomized vector that is symmetric following the 
                                                                                #constrainsts given in pdf
        for i in range(self.n):
            for j in range(self.n):
                if i < j:                                                       #no need to do anything for bottom triangle matrix since
                    continue                                                    # the matrixis symmetric
                if (i == j):
                    self.mat[i][j] = random.uniform(0, self.n)                  # for diagonal elements random value is between (0,n)
                else:
                    self.mat[i][j] = random.random()                            # for upper traingle matrix as well as lower since it is 
                    self.mat[j][i] = self.mat[i][j]                             #symmetric

    def toRowEchelonForm(self):                                                 #converts the matrix to row echelon form

        for i in range(self.n):
            self.mat[i] = (1/self.mat[i][i]) * self.mat[i]                      #divdie the row i by value of element a_{ii}
            j = i + 1
            while j < self.n:                                                   # subtract the just above row from the current row so that values in lower triangle matrix become 0
                self.mat[j] = self.mat[j] + (-1) * (self.mat[j][i]) * self.mat[i]
                j += 1

    def isDRDominant(self):                                                     # return true if matrix is digonally dominant

        for i in range(self.n):
            diag = self.mat[i][i]
            sum = 0
            for j in range(self.n):
                sum += self.mat[i][j]                                           # total sum of the row elements

            if diag < sum - diag:                                               # checking if diagonal element > sum of all other elements for that row
                return False
        
        return True

    def jSolve(self, b, m):                                                     # Finds solution of linear system of equations using Jacobi method

        try:                                                                    #check if matrix is diagonally dominant if not raise exception
            if not self.isDRDominant():
                raise Exception("Not solving beacuse convergence is not gauranteed")
        except Exception as e:
            print(type(e))
            print(e)
            return

        x = [0] * len(b)                                                        #intial vaulue of x 
        e = []
        for k in range(m):
            x_new = [0] * len(b)                                                #the new values of x
            for i in range(self.n):
                sum = 0
                for j in range(self.n):
                    if i == j:
                        continue
                    sum += self.mat[i][j] * x[j]                                # dot product of x and matrix A except diagonal elements

                x_new[i] = (b[i] -sum)/self.mat[i][i]                           # formula for jacobi method
            
            x = copy.deepcopy(x_new)                                            #updating the values of x
            
            ax = np.zeros(self.n)
            for i in range(self.n):
                for j in range(self.n):
                    ax[i] += self.mat[i][j]*x[j]                                # Dot profuct of A and x

            e.append(np.linalg.norm(ax - np.array(b)))                          # error for each iteration

        return (e, x)

    def gsSolve(self, b, m):                                                    # GS method of solving linear system of equations

        try:                                                                    # similar to jacobi method
            if not self.isDRDominant():
                raise Exception("Not solving beacuse convergence is not gauranteed")
        except Exception as e:
            print(type(e))
            print(e)
            return

        x = [0] * len(b)
        e = []
        
        y = np.zeros(self.n)
        for k in range(m):
            for i in range(self.n):
                sum = 0
                for j in range(self.n):
                    if i == j:
                        continue
                    sum += self.mat[i][j] * x[j]                                #the only difference to jacobi method, updated values are 
                                                                                # used 
                x[i] = (b[i] - sum)/self.mat[i][i]

            ax = np.zeros(self.n)
            for i in range(self.n):
                for j in range(self.n):
                    ax[i] += self.mat[i][j]*x[j]

            e.append(np.linalg.norm(ax - np.array(b)))

        return (e, x)


# s = SquareMatrixFloat(3)
# s.sampleSymmetric()
# print(s)
# print(s.isDRDominant())
# (e, x) = s.gsSolve([1, 2, 3], 10)
# print(e)
# print(x)
# s.toRowEchelonForm()
# print(s)