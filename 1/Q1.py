import numpy as np
import matplotlib.pyplot as plt

logFactList = []            #List of Log of factorial

logSterlingList = []        #List of Log of Sterling approximation

logFact = 0                 #Log of factorial

logSterling = 0             #Log of Sterling approximation

ratio = []                  #Stores the ratio of both values 


for i in range(1, 1000000):

    logFact = logFact + np.log( i )         #log(n!)
    logFactList.append(logFact)

    logSterling = 0.5 * np.log(2 * np.pi * i) + i * np.log( i / np.e)   #log(sqrt(2 * pi * n) * (n / e) ** n)
    logSterlingList.append(logSterling)

    ratio.append(logFact/logSterling)       #ratio of both

plt.plot(ratio)                             #plotting the graphs
plt.ylabel("Ratio")
plt.xlabel("n")

plt.show()