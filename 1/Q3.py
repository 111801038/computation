import matplotlib.pyplot as plt
import numpy as np

def estimatePi(t):

    square = 0              #number of points inside square
    circle = 0              #number of points inside circle

    pi = []                 #actual value of pi from np.pi
    estPi = []              #estimated value of pi using monte carlo method

    for i in range(t):
        rX = np.random.rand() - 0.5         #random x co-ordinate of a point
        rY = np.random.rand() - 0.5         #random y co-ordinate of a point

        if(rX**2 + rY**2 <= 0.25):          #if point lies inside circle
            circle += 1
            square += 1
        else:                               #if point lies only inside square
            square += 1

        if(square == 0):                    #handle division by zero
            estPi.append(4 * circle / 0.1)
        else:                               #monte carlo method
            estPi.append(4 * circle / square)

        pi.append(np.pi)

    plt.ylim(3.1, 3.2)                      #plotting the values
    plt.plot(estPi, label = "Monte Carlo Method")
    plt.plot(pi, color = 'r', label = "Value of np.pi")
    plt.legend()
    plt.show()