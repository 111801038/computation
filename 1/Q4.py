import random

class TextGenerator:

    def __init__(self):
        self.dic = {}                       #dictionary of tuple pair as keys and list of next possible words as values

    
    def assimilateText(self, f):

        text = open(f, 'r', encoding='utf-8').read()

        textArray = text.split()            #array of all the words in the file

        dic = {}

        prevPrev = ""                       #prevPrev and prev are the two tuple pair
        prev = ""

        for i in textArray:                 #iterate through the array of words and put the correspoding key and values in dict

            if(prevPrev == ""):
                prevPrev = i
                continue
            if(prev == ""):
                prev = i
                continue
            
            key = prevPrev + " " + prev

            if( key in dic):                #i is the next word that appears after the tuple pair
                if(i not in dic[key]):
                    dic[key].append(i)
            else:
                dic[prevPrev + " " + prev] = [i]
            
            prevPrev = prev
            prev = i
        
        self.dic = dic


    def generateText(self, n, startWord = ""):

        out = ""                            #generated text
        key = ""                            #key for the dictionary

        if (not(startWord == "")):          #user has given a starting word
            
            possibleKeys = []               #list of possible pairs with that startwords
            
            for key in self.dic.keys():     #search for all the possiblilities
                if(key.split()[0] == startWord):
                    possibleKeys.append(key)
            
            if(possibleKeys == []):         #if we couldn't find the starting word
                raise Exception("Uable to produce text with the specified start word")

            key = random.choice(possibleKeys)   #pick one from possibilities at random

            out = key + " "                     #adding it to the output



        for i in range(n - 2):                  #generate the remaining text using keys
            
            if(key == ""):
                key = random.choice(list(self.dic.keys()))
                out = key  + " "

            while(self.dic[key] == []):
                key = random.choice(list(self.dic.keys()))

            currentWord = random.choice(self.dic[key])
            out += currentWord + " "
            key = key.split()[1] + " " + currentWord

        print(out)

d = TextGenerator()
d.assimilateText("1/sherlock.txt")
d.generateText(100)
