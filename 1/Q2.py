import numpy as np
import matplotlib.pyplot as plt

class Dice():
    
    def __init__(self, number):
        
        if(not(isinstance(number, int))):                   #raise exception in case number of faces in not an integer
            raise Exception("Cannot construct the dice")
        
        if(number < 4):
            raise Exception("Cannot construct the  dice")

        self.number = number                                #number of faces
        dist = []
        for i in range(number):                             #setting probability distribution to default values
            dist.append(1.0/number)
        dist = tuple(dist)

        self.dist = dist                                    #probability distribution
        
    def setProb(self, dist):
       
        if(not(isinstance(dist, tuple))):                   #verfying if distribution is correct, if not raise exception
            raise Exception("Invalid Probability Distribution")
        
        if(not(len(dist) == self.number)):
            raise Exception("Invalid Probability Distribution")
        
        sum = 0
        for x in dist:
            sum = sum + x
        
        if(not(sum == 1)):                                  #verifying if distribution is correct, if not raise exception
            raise Exception("Invalid Probability Distribution")

        self.dist = dist

    def roll(self, t):

        freq = np.zeros(self.number)                        #storing outcome of rolls
        exepFreq = []                                       #expected frequencies

        for i in range(t):                                  #roll the dice and update the correspondind value in freq
            r = np.random.rand()
            sum = 0
            for j in range(self.number):
                sum = sum + self.dist[j]
                if(r < sum):
                    freq[j] += 1
                    break
        
        for i in range(self.number):                        #expected freq distribution
            exepFreq.append(t * self.dist[i])

        x = np.arange(1, self.number + 1)                   #plotting the graph
        plt.bar(x - 0.15, freq, width=0.3, label = "result")
        plt.bar(x + 0.15, exepFreq, color = 'r', width=0.3, label = "expected")
        plt.legend()
        plt.show()


    def __str__(self):                                      #for overloading the print function
        return str("Dice with {0} faces and probability distribution {1}".format(self.number, self.dist))