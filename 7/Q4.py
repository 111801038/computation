import numpy as np
import matplotlib.pyplot as plt

'''
f(x) : returns x^3 + 2x^2 - 10x + 5 ie function for which we have to find the roots
'''
def f(x):
    return x**3 + 2*x**2 - 10*x + 5

'''
f_(x) : differentiation of f(x)
'''
def f_(x):
    return 3*x**2 + 4*x - 10

'''
newton(x, k) : newton method for k number of iterations and x as starting value of x, retuns list of x found after each iteration
'''
def newton(x, k):
    
    xs = [x]

    for _ in range(k):
        x = x - (f(x)/f_(x))
        xs.append(x)

    return xs

'''
secant(xk1, xk, k) : secant method for k number of iterations and intial values xk and xk1, retuns list of x found after each iteration
'''
def secant(xk1, xk, k):

    xs = [xk]

    for _ in range(k):
        temp = xk - f(xk) * ((xk - xk1)/(f(xk) - f(xk1) + 1e-5))
        xk1 = xk
        xk = temp
        xs.append(xk)

    return xs

'''
fun(k, x0, x1) : Runs the newton and secant method for f(x) for k iterations and x0 and x1 (only for secant method) as initial value
'''
def fun(k, x0, x1):

    xs = np.arange(0, k+1)
    nr = np.array(newton(x0, k))
    s = np.array(secant(x1, x0, k))

    plt.plot(xs, f(nr), label='Newton-Raphson Method')
    plt.plot(xs, f(s), label='Secant Method')
    plt.xlabel('Number of iterations')
    plt.ylabel('f(x)')
    plt.title('Convergence of Newton-Raphson v/s Secant Method\non $f(x)=x^3 + 2x^2-10x+5$')
    plt.legend()
    plt.grid()
    plt.show()

fun(10, 1, 2)