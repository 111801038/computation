import numpy as np
import scipy.integrate as integrate
import matplotlib.pyplot as plt
import matplotlib.animation as animation

'''
root(n, a, epsilon) : Finds the nth root of a using bisection method with a tolerence of epsilon
'''
def root(n, a, epsilon):
    x = 0
    y = a
                                                                        # Bisection method
    while abs(y - x) > epsilon:                                         # While change is large enough
        z = (y+x)/2                                                     # bisection
        if (np.power(z, n) - a) * (np.power(x, n) - a) > 0:             # positive if both are positive or negative
            x = z
        else:
            y = z
    
    return (x+y)/2

print(root(7, 128, 1e-10))
