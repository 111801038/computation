import matplotlib.pyplot as plt
import numpy as np

class Polynomial:

    def __init__(self, coeff):                                      #intialize the polynomial with a list of coeff and deg
        
        try:                                                        #raise exception in case its not
            if not(type(coeff) == list):
                raise Exception("Coefficients must be in form of a list")
        except Exception as e:
            print(type(e))
            print(e)

        self.coeff = coeff
        self.deg = len(coeff) - 1

    def __str__(self):                                               # overloading the print
        s = "Coefficient of the polynomial are:\n"
        for i in self.coeff:
            s += str(i) + " "
        return s

    def __add__(self, other):                                       # overloading + operator for adding two polynomials 
        coeff = []

        if self.deg < other.deg:                                    # if degree of first is less than the other one
            return other + self                                     # this is because the below statement for adding assumes self.deg >= other.deg

        for i in range(self.deg + 1):                               
            if i > other.deg:                                       # case when degree of first is greater than deg of second and we ran of degrees
                coeff.append(self.coeff[i])
            else:                                                   # normal case of adding the polynomials
                coeff.append(self.coeff[i] + other.coeff[i])

        
        return Polynomial(coeff)

    def __sub__(self, other):                                       # overloading the subtract operator
        coeff = []

        if self.deg < other.deg:                                    # again the same thing as add below code assumes that deg of first > deg of second
            return (other - self)*(-1)                              # a - b = - (b - a)

        for i in range(self.deg + 1):                                       
            if i > other.deg:                                      # case when degree of first were greater we don't need to substract anything
                coeff.append(self.coeff[i])
            else:                                                   # the normal case of subtracting two polynomials
                coeff.append(self.coeff[i] - other.coeff[i])

        return Polynomial(coeff)

    def __mul__(self, other):                                       # overloading the * operator
        coeff = []
        
        try:
            if type(other) == Polynomial:                               # case when other is a ploynomial
                coeff = [0] * (self.deg + other.deg + 1)
                for i in range(self.deg + 1):
                    for j in range(other.deg + 1):
                        coeff[i+j] += self.coeff[i] * other.coeff[j]    #simply multiplying two coeff and placing them in right index
            elif type(other) == int or type(other) == float:            # case when other is a numeric value
                for i in range(self.deg+1):
                    coeff.append(self.coeff[i] * other)
            else:                                                       # exception case
                raise Exception("* operator requies either a polynomial or numeric value")
        except Exception as e:
            print(type(e))
            print(e)    


        return Polynomial(coeff)

    def __rmul__(self, other):                                           # for cases such as 4 * Polynomial
        return self * other


    def __getitem__(self, x):                                            #overloading the [] return the values of polynomial for a values of x

        res = 0

        for i in range(self.deg + 1):
            res += self.coeff[i]*(x**i)

        return res


    def show(self, start, end):                                           # plots the polynomial

        p = []
        for i in np.arange(start, end, (end-start)/100):
            p.append(self[i])

        plt.plot(np.arange(start, end, (end-start)/100), p, label = "Polynomial")
        plt.ylabel("f(x)")
        plt.xlabel("x")
        plt.legend()
        plt.show()
        

    def fitViaMatrixMethod(self, points):                                   # finds the best fir polynomail for given points using matrix method
        
        deg = len(points)

        mat = np.zeros((deg, deg))
        b = np.zeros(deg)

        for i in range(deg):
            for k in range(deg):
                mat[i, k] = np.power(points[i][0], k)                       # generating the matrix for ploynomial equations
            b[i] = points[i][1]

        x = np.linalg.solve(mat, b)                                         # using lingalg for solving the matrix equation

        x = list(x)

        min = points[0][0]                                                  # min and max on x asix for constraints on plot
        max = points[0][0]
        for k in points:
            plt.scatter(k[0], k[1], color = 'r')                            #plotting the given points
            plt.title("Matix method")
            if k[0] > max:
                max = k[0]
            if k[0] < min:
                min = k[0]

        x = Polynomial(x)

        x.show(min - 5, max + 5)                                            # plotting the polynomial

    def fitViaLagrangePoly(self, points):                                   # finds the best fit polynomial using lagrange method
        
        deg = len(points)

        psi = Polynomial([0])
        for j in range(deg):
            psiJ = Polynomial([1])
            for i in range(deg):
                if i == j:                                                  # skip case when i == j
                    continue
                x = Polynomial([-points[i][0], 1])                          # (x - xi)
                x = x*(1/(points[j][0] - points[i][0]))                     # (x - xi)/(xj - xi)
                psiJ = psiJ * x                                             # product of (x - xi)/(xj - xi) for all i
            psi = psi + points[j][1] * psiJ                                 # sum of all psiJ

        min = points[0][0]                                                  # similar to matrix method function
        max = points[0][0]
        for k in points:
            plt.scatter(k[0], k[1], color = 'r')
            plt.title("Lagrange Method")
            if k[0] > max:
                max = k[0]
            if k[0] < min:
                min = k[0]

        psi.show(min - 1, max + 1)
        return psi

    def derivative(self):                                                   # returns derivative of a polynomial
        
        coeff = []

        for i in range(1, self.deg + 1):                                    # ie. ax^2 + bx + c -> 2ax + b
            coeff.append(i* self.coeff[i])                                  # calculating coefficients of derivative oif polynomial

        return Polynomial(coeff)


    def area(self, start, stop):                                            # return area under polynomial from start to end

        try:
            if not(type(start) == int or type(start) == float):
                raise Exception("starting values must be numeric")
            if not(type(stop) == int or type(stop) == float):
                raise Exception("Ending value must be numeric")
        except Exception as e:
            print(type(e))
            print(e)

        coeff = [0]

        for i in range(self.deg + 1):                                       # ie. ax + b -> (a/2)x^2 + bx + 0
            coeff.append(self.coeff[i]/(i+1))                               # calculating coefficients of integral of polynomial

        p = Polynomial(coeff)

        return "Area in the interval [{0}, {1}] is : {2}".format(start, stop, p[stop] - p[start])                                           # g(stop) - g(start) where g is integration of f



    def printRoots(self, limit = 1000):                                     # prints the roots of polynomail using aberth method

        coef = np.array(self.coeff)                                         # getting the coefficients, degree and limits on the roots 
        upperLimit = 1 + max(abs(coef))/(abs(coef[-1]) + 1e-6)
        lowerLimit = 1/((1 + max(abs(coef))/(abs(coef[0])+1e-6)) + 1e-6)
        degree = self.deg

        z = np.random.uniform(lowerLimit, upperLimit, size=(degree))        # starting with random apprroximation

        for _ in range(limit):                                              # aberth method with limit on number of iteration to run for

            zTemp = []

            for i in range(degree):
                s = 0
                for j in range(degree):
                    if i != j:
                        s += 1/((z[i]-z[j]))                                # s = sigma(1/(z[i] - z[j])) where i != j

                p = self[z[i]]                                              # f(x)
                p_ = self.derivative()[z[i]]                                # f'(x)
                zTemp.append(z[i] - (p/p_)/(1 - (p/p_)* s))                 # zk = zk - f(zk)/f'(zk)/(1 - f(zk)/f'(zk).s)
                
            z = zTemp
                                                                            # checking the termination condition
            endLoop = True
            for i in range(degree):
                if self[z[i]] >= 0.001 or self[z[i]] <= -0.001:
                    endLoop = False
            if endLoop:
                break

        return z

'''
fun(f, a, b) : takes a function f, approximates it using Lagrange Polynomial and finds the roots of that polynomial in interval (a, b)
'''
def fun(f, a, b):

    xs = np.linspace(a, b, 10)
    q = []
    for i in xs:
        q.append([float(i), float(f(i))])

    plt.grid()
    P = Polynomial([])
    P = P.fitViaLagrangePoly(q)                                     # Approximating function using Lagrange Polynomial

    roots = P.printRoots()

    rNew = []
                                                                    # finding roots in the interval
    for r in roots:
        if r > a - 0.001 and r < b + 0.001:
            rNew.append(r)

    print("Roots of the function in interval ({1}, {2}) are : {0}".format(rNew, a, b))

# Test Cases
t = lambda x : x*np.cos(x) + np.sin(x)
fun(t, 0, 5)
