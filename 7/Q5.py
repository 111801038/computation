import numpy as np
import matplotlib.pyplot as plt

'''
f(x) : retuns [f1(x), f2(x), f3(x)] where f1(x), f2(x) and f3(x) are the functions given in the quesiton
'''
def f(x):
    
    f1 = 3*x[0] - np.cos(x[1]*x[2]) - 1.5
    f2 = 4*(x[0]**2) - 625*(x[1]**2) + 2*x[2] - 1
    f3 = np.exp(-x[0]*x[1]) + 20*x[2] + 9

    return np.array([f1, f2, f3])

'''
f_(x) : returns the partial derivatives of f1(x), f2(x), f3(x) wrt x1, x2 and x3
'''
def f_(x):

    f1_x1 = 3                                           # patiral derivative of f1 wrt x1
    f1_x2 = x[1] * np.sin(x[1] * x[2])                  # partial derivative of f1 wrt x2
    f1_x3 = x[2] * np.sin(x[1] * x[2])                  # partial derivative of f1 wrt x3

    f2_x1 = 8*x[0]                                      # partial derivatives of f2 wrt x1, x2, x3
    f2_x2 = -1250*x[1]
    f2_x3 = 2

    f3_x1 = -x[1] * np.exp(-x[0] * x[1])                # similarly for f3
    f3_x2 = -x[0] * np.exp(-x[0] * x[1])
    f3_x3 = 20

    return np.array([[f1_x1, f1_x2, f1_x3], [f2_x1, f2_x2, f2_x3], [f3_x1, f3_x2, f3_x3]])

'''
newton(x, k) : runs the newton method for k number of iterations and x as intial value. 
'''
def newton(x, k):

    norms = [np.linalg.norm(f(x))]                      # norm of the function

    for i in range(k):                                  # runs the newton method
        
        J = f_(x)
        x = x - np.dot(np.linalg.inv(J), f(x))
        norms.append(np.linalg.norm(f(x)))
        
    plt.plot(np.arange(0, k+1, 1), norms)               #plot the vlaues
    plt.grid()
    plt.xlabel('Number of iterations')
    plt.ylabel('$|| f(x) ||$')
    plt.title('Newton-Raphson Method for multivariables')
    plt.show()

newton([1, 2, 4], 8)