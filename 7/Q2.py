import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as inte
import matplotlib.animation as animation

'''
Global parameters for the equations
'''
x, h = np.linspace(0, 1, 11, retstep=True)                 # dx = h, sampling xj's from inteval 0, 1
y = np.linspace(0, 1, 11)
u0 = np.zeros((len(x), len(y)))
tspan = np.linspace(0, 0.3, 50)

'''
f(t, u) : takes t and u and returns an array of size len(u), which is used in solve_ivp, i.e. u'(t) = f(t, u)
'''
def f(t, u, xc, yc):
    ft = np.zeros(u0.shape)
    for i in range(1, ft.shape[0] - 1):
        for j in range(1, ft.shape[1] - 1):
            fxy = np.power(np.e, np.sqrt((x[i] - xc)**2 + (y[j] - yc)**2))
            dudx = (u[i-1 + u0.shape[1]*j] - 2*u[i + u0.shape[1]*j] + u[i+1 + u0.shape[1]*j]) / np.power(h, 2)
            dudy = (u[i + u0.shape[1]*(j-1)] - 2*u[i + u0.shape[1]*j] + u[i + u0.shape[1]*(j+1)]) / np.power(h, 2)
            ft[i][j] = dudx + dudy + fxy
    return ft.reshape(u.shape)

'''
Plots parameters and elements
'''
fig = plt.figure()
ax = fig.add_subplot(autoscale_on = True)

line = plt.imshow(u0, cmap ='Oranges')
time_text = ax.text(0.05, 0.9, 'time : %.1fs' % (0), transform=ax.transAxes)

'''
animate(i, sol) : for animation using matplotlib  
'''
def animate(i, q):

    m = np.array(q.sol(i))
    line.set_data(m.reshape(len(x), len(y)))
    time_text.set_text('time : %.1fs' % (i))

    return line, time_text

'''
fun(h, x, u0, tspan) : Solves the ODE using solve_ivp using above functions and saves the animation
'''
def fun(h, x, u0, tspan, xc, yc):

    f_ = lambda t, u : f(t, u, xc, yc)

    sol = inte.solve_ivp(f_, [tspan[0], tspan[-1]], u0, t_eval=tspan, dense_output=True)

    ax.grid()                                                       # aniamting the motion
    plt.xlabel("x")
    plt.ylabel("y")

    ani = animation.FuncAnimation(fig, animate, frames = tspan,fargs=(sol, ), interval = 50)
    plt.colorbar(line)
    ani.save('Q2.gif', writer="pillow", fps=24)
    plt.show()

fun(h, x, u0.reshape(len(x)*len(y)), tspan, 0, 0)