import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as inte
import matplotlib.animation as animation

'''
Global parameters for the equations
'''
x = np.linspace(0, 1, 10)                               # sampling xj's from inteval 0, 1
h = x[1] - x[0]                                         # dx = h
u0 = np.power(np.e, -x)                                 # initial temperature values 
tspan = np.linspace(0, 0.6, 100)                        # sampling time at which to measure the temperatur
u0[0] = 0                                               # boundary conditions
u0[len(u0) - 1] = 0


'''
f(t, u) : takes t and u and returns an array of size len(u), which is used in solve_ivp, i.e. u'(t) = f(t, u)
'''
def f(t, u):
    y = np.zeros(len(u))
    for i in range(1,len(y)-1):
        y[i] = (u[i-1] - 2*u[i] + u[i+1]) / np.power(h, 2)
    return y

'''
Plots parameters and elements
'''
fig = plt.figure()
ax = fig.add_subplot(autoscale_on = True, xlim = (0, 1), ylim = (-1, 1.5))

line, = ax.plot([], [], label = "Heat Equation")
time_text = ax.text(0.05, 0.9, 'time : %.1fs' % (0), transform=ax.transAxes)

'''
animate(i, sol) : for animation using matplotlib  
'''
def animate(i, q):

    y = q.sol(i)
    
    line.set_data(x, y)
    time_text.set_text('time : %.1fs' % (i))

    return line, time_text

'''
fun(h, x, u0, tspan) : Solves the ODE using solve_ivp using above functions and saves the animation
'''
def fun(h, x, u0, tspan):
    sol = inte.solve_ivp(f, [tspan[0], tspan[-1]], u0, t_eval=tspan, dense_output=True)

    ax.grid()                                                       # aniamting the motion
    plt.xlabel("x")
    plt.ylabel("Temperature")

    ani = animation.FuncAnimation(fig, animate, frames = tspan,fargs=(sol, ), interval = 50)

    ani.save('Q1.gif', writer="pillow", fps=24)
    plt.legend()
    plt.show()

fun(h, x, u0, tspan)