import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
from scipy.misc import derivative as der
import matplotlib.animation as animation

L = 1                                                   # Length of the pendulum
h = 0.01                                                # value of h for Euler mehtod
omegaList = []                                          # for storing omegas and thetas respectively
thetaList = []
initialOmega = 0                                        # intial conditions of the pendulum
initialTheta = 1.5
xList = np.arange(0, 4, h)                              # x's for which we have to use Euler mehtod

fig = plt.figure()                                      # plots parameters and elements
ax = fig.add_subplot(111, autoscale_on=True, xlim=(-L - 0.5, L + 0.5), ylim=(-L - 0.5, L + 0.5))
ax.text(0.05, 0.8, 'initial theta: %.1f' % (initialTheta), transform=ax.transAxes)
ax.text(0.05, 0.7, 'intial omega : %.1f' % (initialOmega), transform=ax.transAxes)
bob = ax.scatter(0, -L, lw = 5, color = 'b')
time_text = ax.text(0.05, 0.9, 'time : %.1fs' % (0), transform=ax.transAxes)
line, = ax.plot([0, 0], [0, -L], lw = 2, color = 'r')

'''
f(x) : is defined such that x''(x) = f(x)
'''
def f(x):
    return -10*np.sin(x)/L

'''
trajectory(omega, theta) :  retuns the list of omega and theta that the pendulum will have
                            given intial conditions omega and theta using euler method
'''
def trajectory(omega, theta):

    for i in xList:                                 # froward euler method
        omegaList.append(omega)
        thetaList.append(theta)
        theta = theta + h*omega
        omega = omega + h*f(theta)
    
    omegaT = intp.Akima1DInterpolator(xList, omegaList)     # iterpolating omega and theta
    thetaT = intp.Akima1DInterpolator(xList, thetaList)

'''
animate(i) : for animating the pendulum simulation using matplotlib
'''
def animate(i):
    thisx = [0, -L*np.sin(thetaList[i])]
    thisy = [0, -L*np.cos(thetaList[i])]

    bob.set_offsets([thisx[1], thisy[1]])
    time_text.set_text('time : %.1fs' % (i*h))
    line.set_data(thisx, thisy)
    return line, bob, time_text

'''
fun() : helper funtions that calls the above funcitons in order and draws the animation
'''
def fun():

    trajectory(initialOmega, initialTheta)

    ax.grid()
    plt.xlabel("x")
    plt.ylabel("y")

    ani = animation.FuncAnimation(fig, animate, np.arange(1, len(xList)), interval = 5)

    ani.save('pendulum.gif', fps=30)
    plt.show()

# fun()