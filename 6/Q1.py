import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
from scipy.misc import derivative as der

'''
f(x) : return -2*x ie. x'(t) = -2x(t)
'''
def f(x):
    return -2*x

'''
ft(x) : true integration of the x'(t) 
'''
def ft(x):
    return 5 * np.power(np.e, -2*x)

'''
fun() : Approximates the function x(t) from ODE using forward Euler method
'''
def fun():
    hList = [0.1, 0.5, 1, 2, 3]                                     # Different values of h

    xs = np.arange(0, 10, 0.1)                                      # Sampling x's for plotting

    for h in hList:                                                 # iterating for each h
        
        yList = []
        y = 5
        xList = np.arange(0, 10, h)

        for i in xList:                                             # Forward Euler method
            yList.append(y)
            y = y + h*f(y)
        
        yt = intp.CubicSpline(xList, yList)                         # Using Cubic spline to interpolate the funtion

        plt.plot(xs, yt(xs), label = "For h = {0}".format(h))       # plotting


    plt.plot(xs, ft(xs), label = "true function")                   # plotting the ture x(t)
    plt.ylim(-10, 10)
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("x(t)")
    plt.grid()
    plt.show()

# fun()