import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
import scipy.integrate as inte
from scipy.misc import derivative as der
import matplotlib.animation as animation

# fogure parameters
fig = plt.figure()
ax = fig.add_subplot(111, autoscale_on=True, xlim=(-4, 4), ylim = (-4, 4))

# 3 bodies
body1 = ax.scatter(0, 0, lw = 1, color = 'b')
body2 = ax.scatter(1, 1, lw = 1, color = 'r')
body3 = ax.scatter(1, 0, lw = 1, color = 'g')
'''
animate(i, sol) : for animation the motion of bodies using matplotlib  
'''
def animate(i, sol):

    l = sol(i)
    lPrev = sol(i-1)

    body1.set_offsets([l[0], l[1]])
    body2.set_offsets([l[2], l[3]])
    body3.set_offsets([l[4], l[5]])

    return body1, body2, body3

'''
f(t, y) : for use with solve_ivp, x'(t) = f(t)
'''
def f(t, y):
    
    r = [ np.array([ y[0], y[1] ]), np.array([ y[2], y[3] ]), np.array([ y[4], y[5] ]) ]

    t1 = (r[1] - r[0])/(np.power(np.linalg.norm(r[0] - r[1]) , 3) + 1e-4) + (r[2] - r[0])/(np.power(np.linalg.norm(r[2] - r[0]) , 3) + 1e-4)
    t2 = (r[0] - r[1])/(np.power(np.linalg.norm(r[0] - r[1]) , 3) + 1e-4) + (r[2] - r[1])/(np.power(np.linalg.norm(r[1] - r[2]) , 3) + 1e-4)
    t3 = (r[0] - r[2])/(np.power(np.linalg.norm(r[0] - r[2]) , 3) + 1e-4) + (r[1] - r[2])/(np.power(np.linalg.norm(r[2] - r[1]) , 3) + 1e-4)
    
    ret = list(y[6:])
    ret.append(t1[0])
    ret.append(t1[1])
    ret.append(t2[0])
    ret.append(t2[1])
    ret.append(t3[0])
    ret.append(t3[1])

    return np.array(ret)

'''
fun() : solves the ODE using solve_ivp and animates the motion of the bodies
'''
def fun():
    r = [-1, -1, 1, 3, 2, 1] + [0]*6                             # first 6 values are position vector of 3 bodies and
                                                                    # next 3 are verlocities
    r0 = np.array(r)

    sol = inte.solve_ivp(f, (0, 10), r0, dense_output=True)         # solving the ODE

    ax.grid()                                                       # aniamting the motion
    plt.xlabel("x")
    plt.ylabel("y")

    ani = animation.FuncAnimation(fig, animate, frames = np.arange(0, 20, 0.1),fargs=(sol.sol, ), interval = 30)

    ani.save('threeBodyProblem.gif', writer="pillow", fps=60)
    plt.show()

# fun()







