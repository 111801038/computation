import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
from scipy.misc import derivative as der

'''
f(x) : return -2*x ie. x'(t) = -2x(t)
'''
def f(x):
    return -2*x

'''
ft(x) : true integration of the x'(t) 
'''
def ft(x):
    return 5 * np.power(np.e, -2*x)

'''
fun() : Approximates x(t) from ODE using Backward Euler method
'''
def fun():

    hList = [0.1, 0.5, 1, 2, 3]                             # Different values of h

    xs = np.arange(0, 10, 0.1)                              # sampling x's for plotting

    for h in hList:                                         # iterating through hs
        
        yList = []
        y = 5
        xList = np.arange(0, 10, h)

        for i in xList:                                     # Backward Euler mehtod             
            yList.append(y)
            y = y/(1 + 2*h)
        
        yt = intp.Akima1DInterpolator(xList, yList)         # interpolating the functions

        plt.plot(xs, yt(xs), label = "For h = {0}".format(h))   # plotting

    plt.plot(xs, ft(xs), label = "true function")               # plotting the true function
    plt.legend()
    plt.xlabel("t")
    plt.ylabel("x(t)")
    plt.grid()
    plt.show()

# fun()