import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as intp
import scipy.integrate as inte
from scipy.misc import derivative as der

'''
root(t, y) : for use in solve_ivp for getting the period of the oscillator 
'''
def root(t, y):
    return y[1]

'''
f(u) : retuns a function g(y, x) = [u(1 - x1^2) * x0 - x1, x0] derived from the ODE
'''
def f(u):
    return lambda t, x : np.array([u*(1 - np.power(x[1], 2)) * x[0] - x[1], x[0]])

'''
vanDerPol(u, y0) :  uses solve_ivp on VanDerPol equation for u in uList
                    and initial condition y0 in y0List and finds the period if it is periodic
'''
def vanDerPol(uList, y0List):

    try:
        if not(type(uList) == list):
            raise Exception("uList must be a list even when passing single elements")
        if not(type(y0List) == list):
            raise Exception("y0List must be a list")
        if len(uList) == 0:
            raise Exception("uList cannot be empty")
        if len(y0List) == 0:
            raise Exception("y0List cannot be empty")
        if not(len(uList) == len(y0List)):
            raise Exception("length of both uList and y0List must be same") 
    except Exception as e:
        print(type(e))
        print(e)

    for (u, y0) in zip(uList, y0List):
                                                                    # solve_ivp for solving ODE
        sol = inte.solve_ivp(f(u), (0, 20), y0, dense_output=True, max_step = 0.1, events=root)
        
        if u >= 0:                                                  # finding the period of the function  
            zeros = sol.t_events[0]
            spacing = zeros[1:] - zeros[:-1]
            deltas = spacing[1:] - spacing[:-1]

            timePeriod = spacing[-1]
            print("Time period of the oscillator for u = {0} is {1} ".format(u, 2*timePeriod))
        else:                                                       # if u < 0 then it is not periodic
            print("Not periodic")
                                                                    #   plotting the function
        plt.plot(sol.t, sol.y[1], label = "u = {0} and y0 = {1}".format(u, y0))     
    
    plt.grid()
    plt.xlabel("t")
    plt.ylabel("x(t)")
    plt.legend()
    plt.show()

#Test case
# vanDerPol([0, 1, 2, -1], [np.array([1, 1])]*4)